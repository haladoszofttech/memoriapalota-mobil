package com.google.ar.core.examples.kotlin.helloar.model

data class MemoGetMediaResponse(
    val id: Long,
    val fileName: String,
    val content: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MemoGetMediaResponse

        if (id != other.id) return false
        if (fileName != other.fileName) return false
        if (!content.contentEquals(other.content)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + fileName.hashCode()
        result = 31 * result + content.hashCode()
        return result
    }
}
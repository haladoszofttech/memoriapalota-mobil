package com.google.ar.core.examples.kotlin.helloar

import android.net.Uri

data class VideoListItem (
    val title : String,
    val uri: Uri
)
package com.google.ar.core.examples.kotlin.helloar.model

data class MemoFindMediaResponse(
    val id: Long,
    val name: String
)

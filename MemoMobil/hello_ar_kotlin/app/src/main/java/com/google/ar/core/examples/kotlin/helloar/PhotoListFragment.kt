package com.google.ar.core.examples.kotlin.helloar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.ar.core.examples.kotlin.helloar.viewmodel.PhotoListViewModel
import java.io.File

class PhotoListFragment : Fragment() {

    companion object {
        val URI_KEY = "uri"
    }

    private val viewModel: PhotoListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_photo_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = PhotoListAdapter(PhotoListItemClickListener { uri ->
            val bundle = bundleOf(URI_KEY to uri)
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_photoMenuItem_to_photoFragment, bundle)
        })

        viewModel.files.observe(viewLifecycleOwner) { list ->
            adapter.submitList(list)
        }
        val photosDir = File(requireActivity().filesDir, "Photos")
        viewModel.refresh(photosDir)

        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(view.context)

        val refreshButton = view.findViewById<ImageButton>(R.id.refreshButton)
        refreshButton.setOnClickListener {
            viewModel.refresh(photosDir)
            onResume()
        }

        val downLoadButton = view.findViewById<ImageButton>(R.id.downloadButton)
        downLoadButton.setOnClickListener {
            viewModel.downloadNewFiles(photosDir)
            onResume()
        }
    }
}
package com.google.ar.core.examples.kotlin.helloar.network

import com.google.ar.core.examples.kotlin.helloar.model.MemoFindMediaResponse
import com.google.ar.core.examples.kotlin.helloar.model.MemoGetMediaResponse
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.MultipartBody
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

private const val BASE_URL = "http://ec2-3-121-239-98.eu-central-1.compute.amazonaws.com:8080/memo/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface Service {
    @GET("image")
    suspend fun getAllImages(): List<MemoFindMediaResponse>

    @GET("image/{id}")
    suspend fun getImage(@Path("id") id: Long): MemoGetMediaResponse

    @POST("image")
    @Multipart
    suspend fun uploadImage(@Part file: MultipartBody.Part): Long


    @GET("video")
    suspend fun getAllVideos(): List<MemoFindMediaResponse>

    @GET("video/{id}")
    suspend fun getVideo(@Path("id") id: Long): MemoGetMediaResponse

    @POST("video")
    @Multipart
    suspend fun uploadVideo(@Part file: MultipartBody.Part): Long
}

object MemoApi {
    val retrofitService: Service by lazy {
        retrofit.create(Service::class.java)
    }
}
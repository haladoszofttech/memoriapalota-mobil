package com.google.ar.core.examples.kotlin.helloar

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.ar.core.examples.kotlin.helloar.databinding.PhotoItemBinding

class PhotoListAdapter(
    val onItemListener: PhotoListItemClickListener
) : ListAdapter<PhotoListItem , PhotoListAdapter.PhotoListViewHolder>(PhotoListItemDiffCallback()) {

    inner class PhotoListViewHolder(
        val binding: PhotoItemBinding
    ) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = PhotoItemBinding.inflate(layoutInflater, parent, false)
        return PhotoListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PhotoListViewHolder, position: Int) {
        holder.binding.apply {
            textView.text = getItem(position).title
            item = getItem(position)
            clickListener = onItemListener
        }
    }
}

class PhotoListItemClickListener(
    private val clickListener: (uri: Uri) -> Unit
) {
    fun onClick(uri: Uri) = clickListener(uri)
}

class PhotoListItemDiffCallback : DiffUtil.ItemCallback<PhotoListItem>() {
    override fun areItemsTheSame(oldItem: PhotoListItem, newItem: PhotoListItem): Boolean {
        return oldItem.uri == newItem.uri
    }

    override fun areContentsTheSame(oldItem: PhotoListItem, newItem: PhotoListItem): Boolean {
        return oldItem.title == newItem.title
    }
}
package com.google.ar.core.examples.kotlin.helloar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.ar.core.examples.kotlin.helloar.databinding.ObjectListItemBinding


class ObjectListAdapter(private val objectListItemClickListener: ObjectListItemClickListener)
    : RecyclerView.Adapter<ObjectListAdapter.ViewHolder>() {

    private val dataSet: List<String> = listOf(
        "Pawn", "Teapot", "Lamp"
    )

    class ViewHolder(
        private val objectListItemClickListener: ObjectListItemClickListener,
        private val binding: ObjectListItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(objectName: String) {
            binding.objectName = objectName
            binding.clickListener = objectListItemClickListener
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(viewGroup.context)
        val binding = ObjectListItemBinding
            .inflate(layoutInflater, viewGroup, false)

        return ViewHolder(objectListItemClickListener, binding)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(dataSet[position])
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size
}

class ObjectListItemClickListener(
    private val clickListener: (objectName: String) -> Unit
) {
    fun onClick(objectName: String) = clickListener(objectName)
}
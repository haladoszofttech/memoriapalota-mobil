package com.google.ar.core.examples.kotlin.helloar

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.ar.core.examples.kotlin.helloar.databinding.VideoItemBinding

class GalleryListAdapter(
    val onItemListener: GalleryListItemClickListener
) : ListAdapter<VideoListItem , GalleryListAdapter.GalleryListViewHolder>(GalleryListItemDiffCallback()) {

    inner class GalleryListViewHolder(
        val binding: VideoItemBinding
    ) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = VideoItemBinding.inflate(layoutInflater, parent, false)
        return GalleryListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GalleryListViewHolder, position: Int) {
        holder.binding.apply {
            textView.text = getItem(position).title
            item = getItem(position)
            clickListener = onItemListener
        }
    }
}

class GalleryListItemClickListener(
    private val clickListener: (uri: Uri) -> Unit
) {
    fun onClick(uri: Uri) = clickListener(uri)
}

class GalleryListItemDiffCallback : DiffUtil.ItemCallback<VideoListItem>() {
    override fun areItemsTheSame(oldItem: VideoListItem, newItem: VideoListItem): Boolean {
        return oldItem.uri == newItem.uri
    }

    override fun areContentsTheSame(oldItem: VideoListItem, newItem: VideoListItem): Boolean {
        return oldItem.title == newItem.title
    }
}
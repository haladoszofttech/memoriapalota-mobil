package com.google.ar.core.examples.kotlin.helloar.viewmodel

import android.util.Base64
import android.util.Log
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.ar.core.examples.kotlin.helloar.PhotoListItem
import com.google.ar.core.examples.kotlin.helloar.network.MemoApi
import kotlinx.coroutines.launch
import java.io.File

class PhotoListViewModel : ViewModel() {
    private val _files = MutableLiveData<List<PhotoListItem>>()
    val files: LiveData<List<PhotoListItem>>
        get() = _files

    fun refresh(dir: File) {
        val files = dir.listFiles()
        _files.value = files?.map { file ->
            PhotoListItem(file.nameWithoutExtension, file.toUri())
        }
    }

    fun downloadNewFiles(dir: File) {
        val files = dir.listFiles()
        val fileNames = mutableSetOf<String>()
        if (files != null) {
            fileNames.addAll(files.map { file -> file.name }.toSet())
        }
        if (!dir.exists()) {
            dir.mkdir()
        }

        viewModelScope.launch {
            try{
                val images =  MemoApi.retrofitService.getAllImages()
                for (image in images) {
                    if (image.name !in fileNames) {
                        //val videoResponse = MemoApi.retrofitService.getVideo(video.id)
                        val imageResponse = MemoApi.retrofitService.getImage(image.id)
                        val file = File(dir, image.name)
                        file.appendBytes(Base64.decode(imageResponse.content, Base64.DEFAULT))
                    }
                }
                refresh(dir)
            } catch (e: Exception) {
                Log.e("PhotoListViewModel", "ImageBackendError: ${e.message}")
            }

        }
    }
}
package com.google.ar.core.examples.kotlin.helloar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.ar.core.examples.kotlin.helloar.viewmodel.VideoListViewModel

class VideoListFragment : Fragment() {

    private lateinit var galleryList: List<VideoListItem>
    private val viewModel: VideoListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_video_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = GalleryListAdapter(GalleryListItemClickListener { uri ->
            val bundle = Bundle()
            bundle.putString("path", uri.toString())
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_galleryMenuItem_to_galleryFragment, bundle)
        })

        viewModel.files.observe(viewLifecycleOwner) { list ->
            adapter.submitList(list)
        }
        viewModel.refresh(requireActivity().filesDir)

        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(view.context)

        val refreshButton = view.findViewById<ImageButton>(R.id.refreshButton)
        refreshButton.setOnClickListener {
            viewModel.refresh(requireActivity().filesDir)
            onResume()
        }

//        val downLoadButton = view.findViewById<ImageButton>(R.id.downloadButton)
//        downLoadButton.setOnClickListener {
//            viewModel.downloadNewFiles(requireActivity().filesDir)
//            onResume()
//        }
    }
}
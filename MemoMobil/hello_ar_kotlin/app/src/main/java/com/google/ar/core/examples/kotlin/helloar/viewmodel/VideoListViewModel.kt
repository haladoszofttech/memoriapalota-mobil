package com.google.ar.core.examples.kotlin.helloar.viewmodel

import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.ar.core.examples.kotlin.helloar.VideoListItem
import java.io.File

class VideoListViewModel : ViewModel() {
    private val _files = MutableLiveData<List<VideoListItem>>()
    val files: LiveData<List<VideoListItem>>
        get() = _files

    fun refresh(dir: File) {
        val files = dir.listFiles()
        _files.value = files
            .filter { file -> file.name.contains("mp4") }
            .map { file -> VideoListItem(file.nameWithoutExtension, file.toUri()) }
    }

//    fun downloadNewFiles(dir: File) {
//        val files = dir.listFiles()
//        val fileNames = files.map { file ->
//            file.name
//        }.toSet()
//
//        viewModelScope.launch {
//            // val images =  MemoApi.retrofitService.getAllImages()
//            try {
//                val videos = MemoApi.retrofitService.getAllVideos()
//                for (video in videos) {
//                    if (video.name !in fileNames) {
//                        val videoResponse = MemoApi.retrofitService.getVideo(video.id)
//                        val file = File(dir, video.name)
//                        file.appendBytes(Base64.decode(videoResponse.content, Base64.DEFAULT))
//                    }
//                }
//                refresh(dir)
//            } catch (e: Exception) {
//                Log.e("GalleryListViewModel", "VideoBackendError: ${e.message}")
//            }
//        }
//    }
}
/*
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.ar.core.examples.kotlin.helloar

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import android.widget.ToggleButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.google.ar.core.Config
import com.google.ar.core.RecordingConfig
import com.google.ar.core.Session
import com.google.ar.core.Track
import com.google.ar.core.examples.java.common.helpers.*
import com.google.ar.core.examples.java.common.samplerender.SampleRender
import com.google.ar.core.examples.kotlin.common.helpers.ARCoreSessionLifecycleHelper
import com.google.ar.core.examples.kotlin.helloar.network.MemoApi
import com.google.ar.core.exceptions.*
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.*

class ArFragment : Fragment() {
    companion object {
        private const val TAG = "HelloArActivity"
        val ANCHOR_TRACK_ID = UUID.fromString("53069eb5-21ef-4946-b71c-6ac4979216a6")
        private const val ANCHOR_MIME_TYPE = "application/recording-playback-anchor"
    }

    var isRecording = false

    val snackbarHelper = SnackbarHelper()
    lateinit var tapHelper: TapHelper
    lateinit var surfaceView: GLSurfaceView

    lateinit var arCoreSessionHelper: ARCoreSessionLifecycleHelper
    lateinit var renderer: HelloArRenderer

    val instantPlacementSettings = InstantPlacementSettings()
    val depthSettings = DepthSettings()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_ar, container, false)
    }

    override fun onResume() {
        super.onResume()
        surfaceView.onResume()
    }

    override fun onPause() {
        super.onPause()
        surfaceView.onPause()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Setup ARCore session lifecycle helper and configuration.
        arCoreSessionHelper = ARCoreSessionLifecycleHelper(requireActivity())
        // If Session creation or Session.resume() fails, display a message and log detailed
        // information.
        arCoreSessionHelper.exceptionCallback =
            { exception ->
                val message =
                    when (exception) {
                        is UnavailableUserDeclinedInstallationException ->
                            "Please install Google Play Services for AR"
                        is UnavailableApkTooOldException -> "Please update ARCore"
                        is UnavailableSdkTooOldException -> "Please update this app"
                        is UnavailableDeviceNotCompatibleException -> "This device does not support AR"
                        is CameraNotAvailableException -> "Camera not available. Try restarting the app."
                        else -> "Failed to create AR session: $exception"
                    }
                Log.e(TAG, "ARCore threw an exception", exception)
                snackbarHelper.showError(activity, message)
            }

        surfaceView = view.findViewById<GLSurfaceView>(R.id.surfaceview)
        tapHelper = TapHelper(requireContext()).also { surfaceView.setOnTouchListener(it) }

        // Configure session features, including: Lighting Estimation, Depth mode, Instant Placement.
        arCoreSessionHelper.beforeSessionResume = ::configureSession
        lifecycle.addObserver(arCoreSessionHelper)

        // Set up the Hello AR renderer.
        renderer = HelloArRenderer(this)
        lifecycle.addObserver(renderer)

        // Sets up an example renderer using our HelloARRenderer.
        SampleRender(surfaceView, renderer, requireActivity().assets)

        val list = view.findViewById<RecyclerView>(R.id.objectList)
        list.adapter = ObjectListAdapter(ObjectListItemClickListener { objectName ->
            when (objectName) {
                "Pawn" ->  {
                    renderer.virtualObjectName = "pawn"
                    renderer.virtualObjectTextureName = "pawn_albedo"
                }
                "Teapot" -> {
                    renderer.virtualObjectName = "teapot_s0"
                    renderer.virtualObjectTextureName = "teapot_albedo"
                }
                "Lamp" -> {
                    renderer.virtualObjectName = "desk_lamp"
                    renderer.virtualObjectTextureName = "lamp_Albedo"
                }
            }
            renderer.selectedVirtualObjectChanged = true
        })

        val cancelButton = view.findViewById<ImageButton>(R.id.cancelLastObjectButton)
        cancelButton.setOnClickListener {
            renderer.deleteLastObject = true
        }

        val recordButton = view.findViewById<ToggleButton>(R.id.recordingButton)
        recordButton.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                val date = Calendar.getInstance().time.toString()
                val destination = Uri.fromFile(File(requireActivity().filesDir, "$date.mp4"))
                val anchorTrack = Track(arCoreSessionHelper.session)
                    .setId(ANCHOR_TRACK_ID)
                    .setMimeType(ANCHOR_MIME_TYPE)
                val recordingConfig = RecordingConfig(arCoreSessionHelper.session)
                    .setMp4DatasetUri(destination)
                    .setAutoStopOnPause(false)
                    .addTrack(anchorTrack)
                arCoreSessionHelper.session?.startRecording(recordingConfig)
                isRecording = true
                Toast.makeText(activity, "Recording started", Toast.LENGTH_LONG).show()
            } else {
                arCoreSessionHelper.session?.stopRecording()

//                lifecycleScope.launch {
//                    val files = requireActivity().filesDir.listFiles()
//                    val lastFile = files[files.size-1]
//                    val filePart = MultipartBody.Part.createFormData(
//                        "file", lastFile.name,
//                        RequestBody.create(MediaType.parse("video/*"), lastFile)
//                    )
//                    try {
//                        val id = MemoApi.retrofitService.uploadVideo(filePart)
//                    } catch (e : Exception) {
//                        Log.e(TAG, "Backend error: ${e.message}")
//                        Toast.makeText(activity, "Backend error: ${e.message}", Toast.LENGTH_LONG).show()
//                    }
//                }

                isRecording = false
                Toast.makeText(activity, "Recording stopped, recording is at: ${requireActivity().filesDir.absolutePath}", Toast.LENGTH_LONG).show()
            }
        }

        val takePhotoButton = view.findViewById<ImageButton>(R.id.takePhotoButton)
        takePhotoButton.setOnClickListener {
            renderer.takePhoto = true
            Toast.makeText(context, "Took a photo!", Toast.LENGTH_LONG).show()
        }
    }

    fun uploadPhoto(photo: File) {
        lifecycleScope.launch {
            val filePart = MultipartBody.Part.createFormData(
                "file", photo.name,
                RequestBody.create(MediaType.parse("image/*"), photo)
            )
            try {
                val id = MemoApi.retrofitService.uploadImage(filePart)
            } catch (e : Exception) {
                Log.e(TAG, "Backend error: ${e.message}")
                Toast.makeText(activity, "Backend error: ${e.message}", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        results: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, results)
        if (!CameraPermissionHelper.hasCameraPermission(activity)) {
            // Use toast instead of snackbar here since the activity will exit.
            Toast.makeText(activity, "Camera permission is needed to run this application", Toast.LENGTH_LONG)
                .show()
            if (!CameraPermissionHelper.shouldShowRequestPermissionRationale(activity)) {
                // Permission denied with checking "Do not ask again".
                CameraPermissionHelper.launchPermissionSettings(activity)
            }
        }
    }

    // Configure the session, using Lighting Estimation, and Depth mode.
    fun configureSession(session: Session) {
        session.configure(
            session.config.apply {
                lightEstimationMode = Config.LightEstimationMode.ENVIRONMENTAL_HDR
            }
        )
    }
}
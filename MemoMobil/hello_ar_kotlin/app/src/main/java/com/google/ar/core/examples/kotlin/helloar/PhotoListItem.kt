package com.google.ar.core.examples.kotlin.helloar

import android.net.Uri

data class PhotoListItem (
    val title : String,
    val uri: Uri
)
#ifndef MEMOITEM_H
#define MEMOITEM_H

#include <qopengl.h>
#include <QList>
#include <QVector3D>

class MemoItem
{
public:
    MemoItem();
    virtual const GLfloat *constData() const { return m_data.constData(); }
    virtual int count() const { return m_count; }
    virtual int vertexCount() const { return m_count / 6; }

protected:
    virtual void quad(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat x3, GLfloat y3, GLfloat x4, GLfloat y4);
    virtual void extrude(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2);
    virtual void add(const QVector3D &v, const QVector3D &n);

    QList<GLfloat> m_data;
    int m_count = 0;
};

#endif // MEMOITEM_H

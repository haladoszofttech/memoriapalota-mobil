#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWindow)
{
    vboxlayout = new QVBoxLayout();
    vboxlayout->setContentsMargins(0,0,0,0);
    openglwidget = new OpenGLWidget;
    vboxlayout->addWidget(openglwidget);

    spacer = new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Expanding);
    vboxlayout->addSpacerItem(spacer);
    hboxlayout = new QHBoxLayout();
    hboxlayout->setSpacing(0);
    hboxlayout->setContentsMargins(0,0,0,0);
    vboxlayout->addLayout(hboxlayout);

    button1 = new QPushButton("Gallery");
    button1->setFixedHeight(70);
    hboxlayout->addWidget(button1);
    button2 = new QPushButton("Create");
    button2->setFixedHeight(70);
    hboxlayout->addWidget(button2);
    button3 = new QPushButton("Settings");
    button3->setFixedHeight(70);
    hboxlayout->addWidget(button3);

    this->setLayout(vboxlayout);
}

void MainWindow::deleteWidgets(){   //clean up memory, could be made into a layout deleting function to switch layouts
    deleteButtons();
    delete spacer;
    delete openglwidget;
    delete hboxlayout;
    delete vboxlayout;
}

void MainWindow::deleteButtons(){
    delete button1;
    delete button2;
    delete button3;
}


MainWindow::~MainWindow()
{
    deleteWidgets();
    delete ui;
}


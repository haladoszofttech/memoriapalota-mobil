#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>

#include "openglwidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void deleteWidgets();
    void deleteButtons();

    Ui::MainWindow *ui;

    QVBoxLayout* vboxlayout;
    QHBoxLayout* hboxlayout;
    QSpacerItem* spacer;

    OpenGLWidget* openglwidget;
    QPushButton* button1;   //TODO: atnevezni majd ha megvan a funkcioja
    QPushButton* button2;   //
    QPushButton* button3;   //
};
#endif // MAINWINDOW_H
